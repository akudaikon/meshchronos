// *************************************************************************************************
// Battery voltage measurement functions.
// *************************************************************************************************


// *************************************************************************************************
// Include section
#include "project.h"
#include "display.h"
#include "ports.h"
#include "timer.h"
#include "menu.h"
#include "battery.h"


// *************************************************************************************************
// Prototypes section
void reset_batt_measurement(void);
void battery_measurement(void);

u16 adc12_single_conversion(u16 ref, u16 sht, u16 channel);

// *************************************************************************************************
// Defines section


// *************************************************************************************************
// Global Variable section
struct batt sBatt;

//ADC
u16 adc12_result;
u8  adc12_data_ready;


// *************************************************************************************************
// Extern section


// *************************************************************************************************
// @fn          reset_temp_measurement
// @brief       Reset temperature measurement module.
// @param       none
// @return      none
// *************************************************************************************************
void reset_batt_measurement(void)
{	
	// Start with battery voltage of 3.00V 
	sBatt.voltage = 300;
}


// *************************************************************************************************
// @fn          battery_measurement
// @brief       Init ADC12. Do single conversion of AVCC voltage. Turn off ADC12.
// @param       none
// @return      none
// *************************************************************************************************
void battery_measurement(void)
{
	u16 voltage;
	
	// Convert external battery voltage (ADC12INCH_11=AVCC-AVSS/2)
	//voltage = adc12_single_conversion(REFVSEL_2, ADC12SHT0_10, ADC12SSEL_0, ADC12SREF_1, ADC12INCH_11, ADC12_BATT_CONVERSION_TIME_USEC);
	voltage = adc12_single_conversion(REFVSEL_1, ADC12SHT0_10, ADC12INCH_11);

	// Convert ADC value to "x.xx V"
	// Ideally we have A11=0->AVCC=0V ... A11=4095(2^12-1)->AVCC=4V
	// --> (A11/4095)*4V=AVCC --> AVCC=(A11*4)/4095
	voltage = (voltage * 2 * 2) / 41;  

	// Correct measured voltage with calibration value
	voltage += sBatt.offset;
	
	// Discard values that are clearly outside the measurement range 
	if (voltage > BATTERY_HIGH_THRESHOLD) 
	{
		voltage = sBatt.voltage;
	}
	
	// Filter battery voltage
	sBatt.voltage = ((voltage*2) + (sBatt.voltage*8))/10;

	// If battery voltage falls below low battery threshold, set system flag and modify LINE2 display function pointer
	if (sBatt.voltage < BATTERY_LOW_THRESHOLD) 
	{
		sys.flag.low_battery = 1;
		
		// Set sticky battery icon
		display_symbol(LCD_SYMB_BATTERY, SEG_ON);
	}
	else
	{
		sys.flag.low_battery = 0;

		// Clear sticky battery icon
		display_symbol(LCD_SYMB_BATTERY, SEG_OFF);
	}
	
	// Indicate to display function that new value is available
	display.flag.update_battery_voltage = 1;
}

// *************************************************************************************************
// @fn          display_battery_V
// @brief       Display routine for battery voltage. 
// @param       none
// @return      none
// *************************************************************************************************
void display_battery_V(void)
{
	u8 * str;
	
	// Take a voltage measurement
	battery_measurement();
	
	// Clear the LCD
	clear_display();
	
	// Display result in xx.x format
	str = itoa(sBatt.voltage, 3, 0);
	display_chars(LCD_SEG_L2_2_0, str, SEG_ON);
	display_symbol(LCD_SEG_L2_DP, SEG_ON);
	display_chars(LCD_SEG_L1_3_0, (u8*)"BATT", SEG_ON);
}

// *************************************************************************************************
// @fn          clear_batteryDisplay
// @brief       Cleans up LCD after displaying battery value
// @param       none
// @return      none
// *************************************************************************************************
void clear_batteryDisplay(void)
{
	// Clear up display
	display_symbol(LCD_SEG_L2_DP, SEG_OFF);
}

// *************************************************************************************************
// @fn          adc12_single_conversion
// @brief       Init ADC12. Do single conversion. Turn off ADC12.
// @param       none
// @return      none
// *************************************************************************************************
u16 adc12_single_conversion(u16 ref, u16 sht, u16 channel)
{
	// Initialize the shared reference module 
	REFCTL0 |= REFMSTR + ref + REFON;    		// Enable internal reference (1.5V or 2.5V)
  
	// Initialize ADC12_A 
	ADC12CTL0 = sht + ADC12ON;					// Set sample time 
	ADC12CTL1 = ADC12SHP;                     	// Enable sample timer
	ADC12MCTL0 = ADC12SREF_1 + channel;  		// ADC input channel  
	ADC12IE = 0x001;                          	// ADC_IFG upon conv result-ADCMEMO
  
  	// Wait 2 ticks (66us) to allow internal reference to settle
	Timer0_A4_Delay(2);
	
	// Start ADC12
	ADC12CTL0 |= ADC12ENC;                             		  	

	// Clear data ready flag
  	adc12_data_ready = 0;
  	
  	// Sampling and conversion start  
    ADC12CTL0 |= ADC12SC;                   	
    
    // Wait until ADC12 has finished
    Timer0_A4_Delay(5);       
	while (!adc12_data_ready);
	
	// Shut down ADC12
	ADC12CTL0 &= ~(ADC12ENC | ADC12SC | sht);
	ADC12CTL0 &= ~ADC12ON;
	
	// Shut down reference voltage 	
	REFCTL0 &= ~(REFMSTR + ref + REFON); 
	
	ADC12IE = 0;                          	
	
	// Return ADC result
	return (adc12_result);
}

// *************************************************************************************************
// @fn          ADC12ISR
// @brief       Store ADC12 conversion result. Set flag to indicate data ready.
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector=ADC12_VECTOR
__interrupt void ADC12ISR (void)
{
  switch(__even_in_range(ADC12IV,34))
  {
  case  0: break;                           // Vector  0:  No interrupt
  case  2: break;                           // Vector  2:  ADC overflow
  case  4: break;                           // Vector  4:  ADC timing overflow
  case  6:                                  // Vector  6:  ADC12IFG0
    		adc12_result = ADC12MEM0;                       // Move results, IFG is cleared
    		adc12_data_ready = 1;
    		_BIC_SR_IRQ(LPM3_bits);   						// Exit active CPU
    		break;
  case  8: break;                           // Vector  8:  ADC12IFG1
  case 10: break;                           // Vector 10:  ADC12IFG2
  case 12: break;                           // Vector 12:  ADC12IFG3
  case 14: break;                           // Vector 14:  ADC12IFG4
  case 16: break;                           // Vector 16:  ADC12IFG5
  case 18: break;                           // Vector 18:  ADC12IFG6
  case 20: break;                           // Vector 20:  ADC12IFG7
  case 22: break;                           // Vector 22:  ADC12IFG8
  case 24: break;                           // Vector 24:  ADC12IFG9
  case 26: break;                           // Vector 26:  ADC12IFG10
  case 28: break;                           // Vector 28:  ADC12IFG11
  case 30: break;                           // Vector 30:  ADC12IFG12
  case 32: break;                           // Vector 32:  ADC12IFG13
  case 34: break;                           // Vector 34:  ADC12IFG14
  default: break;
  }
}
