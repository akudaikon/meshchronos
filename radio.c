// *************************************************************************************************
// Radio core access functions. Taken from TI reference code for CC430.
// *************************************************************************************************

// system
#include "project.h"
#include "radio.h"
#include "rf1a.h"
#include "display.h"
#include "smartrf.h"
#include "packet.h"
#include "acceleration.h"
#include "pmm.h"

// *************************************************************************************************
// @fn          radio_config
// @brief       Writes radio configuration to configuration registers.
// @param       none
// @return      none
// *************************************************************************************************
void radio_config(void)
{	
	u8 patable[8] =
	{
		SMARTRF_SETTING_PATABLE0,
		SMARTRF_SETTING_PATABLE1,
		SMARTRF_SETTING_PATABLE2,
		SMARTRF_SETTING_PATABLE3,
		SMARTRF_SETTING_PATABLE4,
		SMARTRF_SETTING_PATABLE5,
		SMARTRF_SETTING_PATABLE6,
		SMARTRF_SETTING_PATABLE7
	};
	
	// IOCFG2: GDO2 signals on RF_RDYn
	rf1a_write_single(IOCFG2, SMARTRF_SETTING_IOCFG2);
	// IOCFG0: GDO0 signals on PA power down signal to control RX/TX switch     
	//rf1a_write_single(IOCFG0D, SMARTRF_SETTING_IOCFG0D);
	// IOCFG1: GDO1 signals on RSSI_VALID
	rf1a_write_single(IOCFG1, SMARTRF_SETTING_IOCFG1); 
	// IOCFG0: GDO0 signals on PA power down signal to control RX/TX switch     
	rf1a_write_single(IOCFG0, SMARTRF_SETTING_IOCFG0);
	// FIFOTHR: RX/TX FIFO Threshold: 33 bytes in TX, 32 bytes in RX         
	rf1a_write_single(FIFOTHR, SMARTRF_SETTING_FIFOTHR);
	// SYNC1: high byte of Sync Word
	rf1a_write_single(SYNC1, SMARTRF_SETTING_SYNC1); 
	// SYNC0: low byte of Sync Word
	rf1a_write_single(SYNC0, SMARTRF_SETTING_SYNC0); 
	// PKTLEN: Packet Length in fixed mode, Maximum Length in variable-length mode
	rf1a_write_single(PKTLEN, SMARTRF_SETTING_PKTLEN);
	// PKTCTRL1: status bytes appended to the packet
	rf1a_write_single(PKTCTRL1, SMARTRF_SETTING_PKTCTRL1); 
	// PKTCTRL0: Fixed-Length Mode, CRC
	rf1a_write_single(PKTCTRL0, SMARTRF_SETTING_PKTCTRL0); 
	// ADDR: Address for packet filtration
	rf1a_write_single(ADDR, SMARTRF_SETTING_ADDR);
	// CHANNR: 8-bit channel number, freq = base freq + CHANNR * channel spacing
	rf1a_write_single(CHANNR, SMARTRF_SETTING_CHANNR);
	// FSCTRL1: Frequency Synthesizer Control (refer to User's Guide/SmartRF Studio)
	rf1a_write_single(FSCTRL1, SMARTRF_SETTING_FSCTRL1);
	// FSCTRL0: Frequency Synthesizer Control (refer to User's Guide/SmartRF Studio)
	rf1a_write_single(FSCTRL0, SMARTRF_SETTING_FSCTRL0);
	// FREQ2: base frequency, high byte
	rf1a_write_single(FREQ2, SMARTRF_SETTING_FREQ2);
	// FREQ1: base frequency, middle byte
	rf1a_write_single(FREQ1, SMARTRF_SETTING_FREQ1);
	// FREQ0: base frequency, low byte      
	rf1a_write_single(FREQ0, SMARTRF_SETTING_FREQ0);
	// MDMCFG4: modem configuration (refer to User's Guide/SmartRF Studio)   
	rf1a_write_single(MDMCFG4, SMARTRF_SETTING_MDMCFG4);
	// MDMCFG3: modem configuration (refer to User's Guide/SmartRF Studio)
	rf1a_write_single(MDMCFG3, SMARTRF_SETTING_MDMCFG3);
	// MDMCFG2: modem configuration (refer to User's Guide/SmartRF Studio)    
	rf1a_write_single(MDMCFG2, SMARTRF_SETTING_MDMCFG2);
	// MDMCFG1: modem configuration (refer to User's Guide/SmartRF Studio)      
	rf1a_write_single(MDMCFG1, SMARTRF_SETTING_MDMCFG1);
	// MDMCFG0: modem configuration (refer to User's Guide/SmartRF Studio)
	rf1a_write_single(MDMCFG0, SMARTRF_SETTING_MDMCFG0);
	// DEVIATN: modem deviation setting (refer to User's Guide/SmartRF Studio)
	rf1a_write_single(DEVIATN, SMARTRF_SETTING_DEVIATN);
	// MCSM2: Main Radio Control State Machine Conf. : timeout for sync word search disabled      
	rf1a_write_single(MCSM2, SMARTRF_SETTING_MCSM2);
	// MCSM1: CCA signals when RSSI below threshold, stay in RX after packet has been received       
	rf1a_write_single(MCSM1, SMARTRF_SETTING_MCSM1);
	// MCSM0: Auto-calibrate when going from IDLE to RX or TX (or FSTXON )
	rf1a_write_single(MCSM0, SMARTRF_SETTING_MCSM0);
	// FOCCFG: Frequency Offset Compensation Conf.
	rf1a_write_single(FOCCFG, SMARTRF_SETTING_FOCCFG);
	// BSCFG: Bit Synchronization Conf.
	rf1a_write_single(BSCFG, SMARTRF_SETTING_BSCFG);    
	// AGCCTRL2: AGC Control    
	rf1a_write_single(AGCCTRL2, SMARTRF_SETTING_AGCCTRL2);
	// AGCCTRL1: AGC Control    
	rf1a_write_single(AGCCTRL1, SMARTRF_SETTING_AGCCTRL1); 
	// AGCCTRL0: AGC Control   
	rf1a_write_single(AGCCTRL0, SMARTRF_SETTING_AGCCTRL0);
	// WOREVT1: High Byte Event0 Timeout    
	rf1a_write_single(WOREVT1, SMARTRF_SETTING_WOREVT1); 
	// WOREVT0: High Byte Event0 Timeout    
	rf1a_write_single(WOREVT0, SMARTRF_SETTING_WOREVT0); 
	// WORCTL: Wave On Radio Control ****Feature unavailable in PG0.6****
	rf1a_write_single(WORCTRL, SMARTRF_SETTING_WORCTRL); 
	// FREND1: Front End RX Conf.
	rf1a_write_single(FREND1, SMARTRF_SETTING_FREND1);
	// FREND0: Front End TX Conf.    
	rf1a_write_single(FREND0, SMARTRF_SETTING_FREND0);
	// FSCAL3: Frequency Synthesizer Calibration (refer to User's Guide/SmartRF Studio)
	rf1a_write_single(FSCAL3, SMARTRF_SETTING_FSCAL3); 
	// FSCAL2: Frequency Synthesizer Calibration (refer to User's Guide/SmartRF Studio)    
	rf1a_write_single(FSCAL2, SMARTRF_SETTING_FSCAL2); 
	// FSCAL1: Frequency Synthesizer Calibration (refer to User's Guide/SmartRF Studio)      
	rf1a_write_single(FSCAL1, SMARTRF_SETTING_FSCAL1); 
	// FSCAL0: Frequency Synthesizer Calibration (refer to User's Guide/SmartRF Studio)     
	rf1a_write_single(FSCAL0, SMARTRF_SETTING_FSCAL0);
	//       
	rf1a_write_single(FSTEST, SMARTRF_SETTING_FSTEST);
	//
	rf1a_write_single(TEST2, SMARTRF_SETTING_TEST2);
	//
	rf1a_write_single(TEST1, SMARTRF_SETTING_TEST1);
	//
	rf1a_write_single(TEST0, SMARTRF_SETTING_TEST0);
	
	rf1a_write_patable(patable);

}


// *************************************************************************************************
// @fn          radio_reset
// @brief       Reset radio core. 
// @param       none
// @return      none
// *************************************************************************************************
void radio_reset(void)
{
	volatile u16 i;
	u8 x;
	
	// Reset radio core
	rf1a_strobe(RF_SRES);
	
	// Wait before checking IDLE 
	for (i=0; i<1000; i++);
	do {
		x = rf1a_strobe(RF_SIDLE);
	} while (x & 0xF0);
	
	// Clear radio error register
	RF1AIFERR = 0;
}


// *************************************************************************************************
// @fn          radio_powerdown
// @brief       Put radio to SLEEP mode. 
// @param       none
// @return      none
// *************************************************************************************************
void radio_powerdown(void)
{
	u8 status = 0;
	// Powerdown radio
	status = rf1a_strobe(RF_SIDLE);
	while (status & 0xF0)
	{
		status = rf1a_strobe(RF_SNOP);
	} 
	rf1a_strobe(RF_SXOFF);
}

// *************************************************************************************************
// @fn          radio_powerdown
// @brief       Puts the radio into RX mode.
// @param       none
// @return      none
// *************************************************************************************************
void radio_receive_on(void)
{
	u8 status = 0;
	
	// get radio into idle state
	status = rf1a_strobe(RF_SIDLE);
	while (status & 0xF0)
	{
		status = rf1a_strobe(RF_SNOP);
	}
	
	// clear rx fifo
	status = rf1a_strobe(RF_SFRX | RF_RXSTAT);
	while (status & 0x0F)
	{
		status = rf1a_strobe(RF_SNOP | RF_RXSTAT);
	}

	// turn on receive interrupt
	RF1AIFG &= ~BITA;
  	RF1AIE  |= BITA;

	// get into rx mode
	status = rf1a_strobe(RF_SRX);
	while ((status & 0xF0) != 0x10)
	{
		status = rf1a_strobe(RF_SNOP);
	}
}

// *************************************************************************************************
// @fn          radio_start
// @brief       Prepare radio for RF communication. 
// @param       none
// @return      none
// *************************************************************************************************
void radio_start(void)
{		
	// Reset radio core
	radio_reset();
	radio_config();

	// goto receive mode
	radio_receive_on();
}


// *************************************************************************************************
// @fn          radio_stop
// @brief       Shutdown radio for RF communication. 
// @param       none
// @return      none
// *************************************************************************************************
void radio_stop(void)
{
	// Disable radio IRQ
	RF1AIFG = 0;
	RF1AIE  = 0; 
		
	// Reset radio core
	radio_reset();
	
	// Put radio to sleep
	radio_powerdown();
	
	// Turn off radio segments
	display_symbol(LCD_ICON_BEEPER1, SEG_OFF);
	display_symbol(LCD_ICON_BEEPER2, SEG_OFF);
	display_symbol(LCD_ICON_BEEPER3, SEG_OFF);
}

// *************************************************************************************************
// @fn          radio_transmit
// @brief       Put radio to SLEEP mode. 
// @param       data		Data to be transmitted
//				length		Length of data to be transmitted, in bytes
// @return      none
// *************************************************************************************************
void radio_transmit(u8 *data, u8 length)
{	
	u8 status = 0;
	
	// get radio into idle state
	status = rf1a_strobe(RF_SIDLE);
	while (status & 0xF0)
	{
		status = rf1a_strobe(RF_SNOP);
	}

	// clear tx fifo
	status = rf1a_strobe(RF_SFTX | RF_TXSTAT);
	while ((status & 0x0F) != 0x0F)
	{
		status = rf1a_strobe(RF_SNOP | RF_TXSTAT);
	}
	
	// write data into fifo
	rf1a_write_tx(data, length);
	
	// enable transmit interrupt
	RF1AIES |= BIT9;                          
	RF1AIFG &= ~BIT9;
	RF1AIE |= BIT9;
	
	// get radio into tx state
	status = rf1a_strobe(RF_STX);
	while ((status & 0xF0) != 0x20)
	{
		status = rf1a_strobe(RF_SNOP);
	}
}

// *************************************************************************************************
// @fn          radio_receive
// @brief       Read received data from RX buffer of radio.
// @param       data		Pointer to where to store received data
//				length		Length of received data to get
// @return      none
// *************************************************************************************************
void radio_receive(u8 *data, u8 length)
{
	// get packet
	rf1a_read_rx(data, length);

	// goto receive mode
	radio_receive_on();
}

// *************************************************************************************************
// @fn          radio_transmitted
// @brief       Clear the transmitted flag and put radio back into RX mode. 
// @param       none
// @return      none
// *************************************************************************************************
void radio_transmitted(void)
{	
	request.flag.radio_transmitted = 0;
	// this will clear rx/tx fifo
	radio_receive_on();
}

// *************************************************************************************************
// @fn          radio_received
// @brief       Handles received data.
// @param       none
// @return      none
// *************************************************************************************************
void radio_received(void)
{
	u8 * strReceiv;
	u8 * strSrc;
	packet_t packet = {0};
	
	radio_receive((u8*)&packet, PACKET_RECEIVE_LENGTH);
	
	// Is the packet's CRC okay?
	// TODO: CRC doesn't seem to be working correctly??
//	if (packet.status.crc_ok) 
//	{
		// Is this packet for me?
		if (packet.header.dst == nodeNum || packet.header.dst == PACKET_DST_ALL)
		{
			switch (packet.header.type)
			{
				case PACKET_TYPE_TEST:
					// Display received data along with source node number
					strReceiv = itoa(packet.payload.accel.xAccel, 4, 0);	
					display_chars(LCD_SEG_L1_3_0, strReceiv, SEG_ON);
					display_chars(LCD_SEG_L2_4_0, (u8*)"RX   ", SEG_ON);
					strSrc = itoa(packet.header.src, 2, 0);
					display_chars(LCD_SEG_L2_1_0, strSrc, SEG_ON);
					
					// Display signal strength of received packet with LCD radio segments
					display_signalStrength(packet.status.rssi);
					break;
				case PACKET_TYPE_DATA:
					// We don't care about other node's data packets. Ignore!
					break;
				case PACKET_TYPE_PING:
					// Send an ACK back to node that pinged us
					radio_sendAck(packet.header.src);
					break;
				case PACKET_TYPE_ACK:
					// Set flag saying we got an ACK back
					request.flag.radio_ack = 1;
					break;
				default:
					// It's not a packet we know of, so just ignore it!
					break;
			}
		}
//	}
	
	request.flag.radio_received = 0;
	radio_start();
}

// *************************************************************************************************
// @fn          radio_sendTest
// @brief       Transmits a test packet with given params
// @param       dstNode		Destination node address
//				testData	Test data to send (8 bits max)
// @return      none
// *************************************************************************************************
void radio_sendTest(u8 dstNode, u8 testData)
{
	packet_t packet = {0};
	
	radio_start();
	
	// Form the test packet to TX
	packet.header.dst = dstNode;
	packet.header.src = nodeNum;
	packet.header.type = PACKET_TYPE_TEST;
	
	// Put test data in test packet
	packet.payload.raw[0] = testData;
	
	// Send packet
	radio_transmit((u8*)&packet, PACKET_TRANSMIT_LENGTH);
	
	// Wait for packet to be sent
	do { idle(); } while (!request.flag.radio_transmitted);
	
	// Clear TX flag
	request.flag.radio_transmitted = 0;	
}

// *************************************************************************************************
// @fn          radio_sendAccel
// @brief       Transmits a data packet containing current accelerometer data
// @param       none
// @return      none
// *************************************************************************************************
void radio_sendAccel()
{
	packet_t packet = {0};
		
	radio_start();
	
	// Form the packet to TX
	packet.header.dst = PACKET_DST_ALL;
	packet.header.src = nodeNum;
	packet.header.type = PACKET_TYPE_DATA;
	
	packet.payload.accel.xAccel = sAccel.xyz[0];
	packet.payload.accel.yAccel = sAccel.xyz[1];
	packet.payload.accel.zAccel = sAccel.xyz[2];

	// Send packet
	radio_transmit((u8*)&packet, PACKET_TRANSMIT_LENGTH);
	
	// Wait for packet to be transmitted
	do { idle(); } while (!request.flag.radio_transmitted);
	
	// Clear TX flag
	request.flag.radio_transmitted = 0;
}

// *************************************************************************************************
// @fn          radio_sendPing
// @brief       Send a ping packet to a node 
// @param       dstNode		Destination node address
// @return      none
// *************************************************************************************************
void radio_sendPing(u8 dstNode)
{
	packet_t packet = {0};
	
	radio_start();
	
	// Form the ping packet to TX
	packet.header.dst = dstNode;
	packet.header.src = nodeNum;
	packet.header.type = PACKET_TYPE_PING;
	
	// Send packet
	radio_transmit((u8*)&packet, PACKET_TRANSMIT_LENGTH);
	
	// Wait for packet to be sent
	do { idle(); } while (!request.flag.radio_transmitted);
	
	// Clear TX flag
	request.flag.radio_transmitted = 0;
}

// *************************************************************************************************
// @fn          radio_sendAck
// @brief       Send ACK to node.
// @param       dstNode		Destination node address
// @return      none
// *************************************************************************************************
void radio_sendAck(u8 dstNode)
{
	packet_t packet = {0};
	
	radio_start();
	
	// Form the ack packet to TX
	packet.header.dst = dstNode;
	packet.header.src = nodeNum;
	packet.header.type = PACKET_TYPE_ACK;
	
	// Send packet
	radio_transmit((u8*)&packet, PACKET_TRANSMIT_LENGTH);
	
	// Wait for packet to be sent
	do { idle(); } while (!request.flag.radio_transmitted);
	
	// Clear TX flag
	request.flag.radio_transmitted = 0;
}

// *************************************************************************************************
// @fn          radio_status
// @brief       Return the current state the radio is in.
// @param       none
// @return      u8		Radio state byte
// *************************************************************************************************
u8 radio_status()
{
	return rf1a_strobe(RF_SNOP);
}

// *************************************************************************************************
// @fn          display_signalStrength
// @brief       Displays the signal strength via radio LCD segments 
// @param       u8		RSSI from RX packet
// @return      none
// *************************************************************************************************
void display_signalStrength(u8 rssi)
{
	// Cast RSSI as signed since it should be
	s8 trueRSSI = (s8)rssi;
	
	display_symbol(LCD_ICON_BEEPER1, SEG_ON);
	display_symbol(LCD_ICON_BEEPER2, SEG_OFF);
	display_symbol(LCD_ICON_BEEPER3, SEG_OFF);
	
	if (trueRSSI > -30)
		display_symbol(LCD_ICON_BEEPER2, SEG_ON);
	if (trueRSSI > 0)
		display_symbol(LCD_ICON_BEEPER3, SEG_ON);	
}

// *************************************************************************************************
// @fn          clear_signalStrength
// @brief       Clear LCD of anything left behind from showing signal strength
// @param       none
// @return      none
// *************************************************************************************************
void clear_signalStrength(void)
{
	display_symbol(LCD_ICON_BEEPER1, SEG_OFF);
	display_symbol(LCD_ICON_BEEPER2, SEG_OFF);
	display_symbol(LCD_ICON_BEEPER3, SEG_OFF);
}

// *************************************************************************************************
// @fn          CC1101_ISR
// @brief       Radio's ISR 
// @param       none
// @return      none
// *************************************************************************************************
#pragma vector=CC1101_VECTOR
__interrupt void CC1101_ISR(void)
{
	u16 rf1aiv = RF1AIV;
	
	if (rf1aiv == RF1AIV_RFIFG9)
	{
		RF1AIES &= ~BIT9; 
		RF1AIFG &= ~BIT9;
		RF1AIE &= ~BIT9;
		request.flag.radio_transmitted = 1;
	}
	else if (rf1aiv == RF1AIV_RFIFG10)
	{
		RF1AIFG &= ~BITA;
		RF1AIE &= ~BITA;
		request.flag.radio_received = 1;
	}

	// Exit from LPM3/LPM4 on RETI
	__bic_SR_register_on_exit(LPM4_bits); 
}
