// *************************************************************************************************
// Include section
#include "project.h"

#include "pmm.h"
#include "vti_as.h"
#include "ports.h"
#include "display.h"
#include "acceleration.h"
#include "battery.h"
#include "timer.h"
#include "menu.h"
#include "radio.h"

// *************************************************************************************************
// FUNCTIONS
void init_application(void);
void init_global_variables();
void wakeup_event(void);
void process_requests(void);

// *************************************************************************************************
// Global Variable section

// global flags
device_t device;

// Variable holding system internal flags
volatile s_system_flags sys;

// Variable holding flags set by logic modules 
volatile s_request_flags request;

// Variable holding display flags
volatile s_display_flags display;

// Variable to hold node's address
volatile u8 nodeNum;

// *************************************************************************************************
// @fn          main
// @brief       Main loop
// @param       none
// @return      none
// *************************************************************************************************
int main(void){

	// Init MCU 
	init_application();

	// Assign initial value to global variables
	init_global_variables();
	
	// Main control loop: wait in low power mode until some event needs to be processed
	for(;;)
	{
		// When idle go to LPM3, when accel TX not active
    	if (!sys.flag.tx_active) idle();

    	// Process wake-up events
    	if (button.all_flags || sys.all_flags) wakeup_event();
    	
    	// Process actions requested by logic modules
    	if (request.all_flags) process_requests();
    	
    	// Send accel data packet if accel TX mode is active and we have a new accel measurement
    	if (sys.flag.tx_active && display.flag.update_acceleration) radio_sendAccel();
    	
    	// If we're on the idle screen and we get a new acceleometer measurement, update the screen
    	if (ptrMenu == &menu_idle && display.flag.update_acceleration) ptrMenu->display_function();
 	}
}

// *************************************************************************************************
// @fn          init_application
// @brief       Initialize the microcontroller.
// @param       none
// @return      none
// *************************************************************************************************
void init_application(void)
{
	volatile unsigned char *ptr;
	  
	// ---------------------------------------------------------------------
	// Enable watchdog
	
	// Watchdog triggers after 16 seconds when not cleared
	// Defined in project.h - currently disabled
	#ifdef USE_WATCHDOG
		WDTCTL = WDTPW + WDTIS__512K + WDTSSEL__ACLK;
	#else
		WDTCTL = WDTPW + WDTHOLD;
	#endif
	
	// ---------------------------------------------------------------------
	// Configure PMM
	SetVCore(3);
	
	// Set global high power request enable
	PMMCTL0_H  = 0xA5;
	PMMCTL0_L |= PMMHPMRE;
	PMMCTL0_H  = 0x00;	

	// ---------------------------------------------------------------------
	// Enable 32kHz ACLK	
	P5SEL |= 0x03;                            // Select XIN, XOUT on P5.0 and P5.1
	UCSCTL6 &= ~(XT1OFF + XT1DRIVE_3);        // XT1 On, Lowest drive strength
	UCSCTL6 |= XCAP_3;                        // Internal load cap

	UCSCTL3 = SELA__XT1CLK;                   // Select XT1 as FLL reference
	UCSCTL4 = SELA__XT1CLK | SELS__DCOCLKDIV | SELM__DCOCLKDIV;      
	
	// ---------------------------------------------------------------------
	// Configure CPU clock for 12MHz
	_BIS_SR(SCG0);                  // Disable the FLL control loop
	UCSCTL0 = 0x0000;          // Set lowest possible DCOx, MODx
	UCSCTL1 = DCORSEL_5;       // Select suitable range
	UCSCTL2 = FLLD_1 + 0x16E;  // Set DCO Multiplier
	_BIC_SR(SCG0);                  // Enable the FLL control loop

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 8 MHz / 32,768 Hz = 250000 = MCLK cycles for DCO to settle
    __delay_cycles(250000);
  
	// Loop until XT1 & DCO stabilizes, use do-while to insure that 
	// body is executed at least once
	do
	{
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + XT1HFOFFG + DCOFFG);
		SFRIFG1 &= ~OFIFG;                      // Clear fault flags
	} while ((SFRIFG1 & OFIFG));	

	
	// ---------------------------------------------------------------------
	// Configure port mapping
	
	// Disable all interrupts
	__disable_interrupt();
	// Get write-access to port mapping registers:
	PMAPPWD = 0x02D52;
	// Allow reconfiguration during runtime:
	PMAPCTL = PMAPRECFG;

	// P2.7 = TA0CCR1A or TA1CCR0A output (buzzer output)
	ptr  = &P2MAP0;
	*(ptr+7) = PM_TA1CCR0A;
	P2OUT &= ~BIT7;
	P2DIR |= BIT7;

	// P1.5 = SPI MISO input
	ptr  = &P1MAP0;
	*(ptr+5) = PM_UCA0SOMI;
	// P1.6 = SPI MOSI output
	*(ptr+6) = PM_UCA0SIMO;
	// P1.7 = SPI CLK output
	*(ptr+7) = PM_UCA0CLK;

	// Disable write-access to port mapping registers:
	PMAPPWD = 0;
	// Re-enable all interrupts
	__enable_interrupt();

	// ---------------------------------------------------------------------
	// Reset radio core
	radio_reset();
	radio_powerdown();
	
	// ---------------------------------------------------------------------
	// Init acceleration sensor
	as_init();
	
	// ---------------------------------------------------------------------
	// Init LCD
	lcd_init();
  
	// ---------------------------------------------------------------------
	// Init buttons
	init_buttons();

	// ---------------------------------------------------------------------
	// Configure Timer0 for use by the clock and delay functions
	Timer0_Init();
	
	// ---------------------------------------------------------------------
	// Start the radio in RX mode
	radio_start();
}


// *************************************************************************************************
// @fn          init_global_variables
// @brief       Initialize global variables.
// @param       none
// @return      none
// *************************************************************************************************
void init_global_variables(void)
{	
	// set device descriptor table
	device = **(device_t**)(0x0ff4);
	
	// Set node number to 1 by default
	nodeNum = 1;

	// Init system flags
	button.all_flags 	= 0;
	sys.all_flags 		= 0;
	request.all_flags 	= 0;
	display.all_flags 	= 0;
	
	// Reset acceleration measurement
	reset_acceleration();
	
	// Reset battery measurement
	reset_batt_measurement();
	battery_measurement();
	
	// Display idle screen
	ptrMenu = &menu_idle;
	ptrMenu->display_function();
}


// *************************************************************************************************
// @fn          wakeup_event
// @brief       Process external / internal wakeup events.
// @param       none
// @return      none
// *************************************************************************************************
void wakeup_event(void)
{
	// Process single button press event (after button was released)
	if (button.all_flags)
	{
		// M1 button event ---------------------------------------------------------------------
		// Select current menu item
		if(button.flag.m1) 
		{		
			ptrMenu->mx_function();
	
			// Clear button flag
			button.flag.m1 = 0;
		}
		// M2 button event ---------------------------------------------------------------------
		// Exit back to main idle screen
		else if(button.flag.m2) 
		{	
			// Return back to idle screen 
			ptrMenu = &menu_idle;
			ptrMenu->display_function();
			
			// Clear button flag
			button.flag.m2 = 0;
		}	
		// S1 button event ---------------------------------------------------------------------
		// Advance to next menu item/Advance setting
		else if(button.flag.s1) 	
		{
			ptrMenu->sx_function(1);
	
			// Clear button flag	
			button.flag.s1 = 0;
		}			
		// S2 button event ---------------------------------------------------------------------
		// Previous to next menu item/Previous setting
		else if(button.flag.s2) 	
		{
			ptrMenu->sx_function(2);
	
			// Clear button flag	
			button.flag.s2 = 0;
		}			
	}
}

// *************************************************************************************************
// @fn          process_requests
// @brief       Process requested actions outside ISR context.
// @param       none
// @return      none
// *************************************************************************************************
void process_requests(void)
{	
	// Do radio receive
	if (request.flag.radio_received) radio_received();
	
	// Do acceleration measurement
	if (request.flag.acceleration_measurement) do_acceleration_measurement();
	
	// Do voltage measurement
	if (request.flag.voltage_measurement) battery_measurement();
	
	// Reset request flag
	request.all_flags = 0;
}
