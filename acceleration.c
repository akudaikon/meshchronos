// *************************************************************************************************
// Accelerometer functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section
#include "project.h"
#include "display.h"
#include "vti_as.h"
#include "pmm.h"
#include "acceleration.h"
#include "packet.h"
#include "radio.h"


// *************************************************************************************************
// Global Variable section
struct accel sAccel;

// Conversion values from data to mgrav taken from CMA3000-D0x datasheet (rev 0.4, table 4)
const u16 mgrav_per_bit[7] = { 18, 36, 71, 143, 286, 571, 1142 };

// *************************************************************************************************
// Extern section

// Global flag for proper acceleration sensor operation
extern u8 as_ok;

// *************************************************************************************************
// @fn          reset_acceleration
// @brief       Reset acceleration variables.
// @param      	none
// @return      none
// *************************************************************************************************
void reset_acceleration(void)
{
	// Start with Y-axis display
	sAccel.view_style 	= DISPLAY_ACCEL_Y;

	// Default mode is off
	sAccel.mode			= ACCEL_MODE_OFF;
}



// *************************************************************************************************
// @fn          switchAccelDisplay
// @brief       Switches between displaying X/Y/Z values.
// @param       none
// @return      none
// *************************************************************************************************
void switchAccelDisplay(void)
{
	if (++sAccel.view_style > 2) sAccel.view_style = 0;
}


// *************************************************************************************************
// @fn          acceleration_value_is_positive
// @brief       Returns 1 if 2's complement number is positive
// @param       u8 value	2's complement number
// @return      u8			1 = number is positive, 0 = number is negavtive
// *************************************************************************************************
u8 acceleration_value_is_positive(u8 value)
{
	return ((value & BIT7) == 0);
}


// *************************************************************************************************
// @fn          convert_acceleration_value_to_mgrav
// @brief       Converts measured value to mgrav units
// @param       u8 value	g data from sensor 
// @return      u16			Acceleration (mgrav)
// *************************************************************************************************
u16 convert_acceleration_value_to_mgrav(u8 value)
{
	u16 result;
	u8 i;
	
	if (!acceleration_value_is_positive(value))
	{
		// Convert 2's complement negative number to positive number
		value = ~value;
		value += 1;
	}
	
	result = 0;
	for (i=0; i<7; i++)
	{
		result += ((value & (BIT(i)))>>i) * mgrav_per_bit[i];
	}
	
	return (result);
}



// *************************************************************************************************
// @fn          is_acceleration_measurement
// @brief       Returns 1 if acceleration is currently measured.
// @param       none
// @return      u8		1 = acceleration measurement ongoing
// *************************************************************************************************
u8 is_acceleration_measurement(void)
{
	return (sAccel.mode == ACCEL_MODE_ON);
}



// *************************************************************************************************
// @fn          do_acceleration_measurement
// @brief       Get sensor data and store in sAccel struct
// @param       none
// @return      none
// *************************************************************************************************
void do_acceleration_measurement(void)
{
	// Get data from sensor
	as_get_data(sAccel.xyz);
	
	// Set display update flag
	display.flag.update_acceleration = 1;
}


// *************************************************************************************************
// @fn          display_acceleration
// @brief       Display routine.
// @param       xData		X-axis value
//				yData		Y-axis value
//				zData		Z-axis value
// @return      none
// *************************************************************************************************
void display_acceleration(u8 xData, u8 yData, u8 zData)
{
	u8 * str;
	u8 raw_data;
	u16 accel_data;

	// Show warning if acceleration sensor was not initialised properly
	if (!as_ok)
	{
		display_chars(LCD_SEG_L1_2_0, (u8*)"ERR", SEG_ON);
	}
	else
	{
		// Convert X/Y/Z values to mg
		switch (sAccel.view_style)
		{
			case DISPLAY_ACCEL_X:
				raw_data = xData;
				display_char(LCD_SEG_L1_3, 'X', SEG_ON);
				break;
			case DISPLAY_ACCEL_Y: 
				raw_data = yData;
				display_char(LCD_SEG_L1_3, 'Y', SEG_ON);
				break;
			default:
				raw_data = zData;
				display_char(LCD_SEG_L1_3, 'Z', SEG_ON);
				break;
		}
		accel_data = convert_acceleration_value_to_mgrav(raw_data) / 10;
		
		// Filter acceleration
		accel_data = (u16)((accel_data * 0.2) + (sAccel.data * 0.8));
		
		// Store average acceleration
		sAccel.data = accel_data;	
		
		// Display decimal point
		display_symbol(LCD_SEG_L1_DP1, SEG_ON);

		// Display acceleration in x.xx format
		str = itoa(accel_data, 3, 0);
		display_chars(LCD_SEG_L1_2_0, str, SEG_ON);
		
		// Display sign
		if (acceleration_value_is_positive(raw_data)) 
		{
			display_symbol(LCD_SYMB_ARROW_UP, SEG_ON);
			display_symbol(LCD_SYMB_ARROW_DOWN, SEG_OFF);
		}
		else 
		{
			display_symbol(LCD_SYMB_ARROW_UP, SEG_OFF);
			display_symbol(LCD_SYMB_ARROW_DOWN, SEG_ON);
		}
	}
}



// *************************************************************************************************
// @fn          clear_accelDisplay
// @brief       Clear LCD of anything left behind from showing accelerometer values
// @param       none
// @return      none
// *************************************************************************************************
void clear_accelDisplay(void)
{
	// Clean up display
	display_symbol(LCD_SEG_L1_DP1, SEG_OFF);
	display_symbol(LCD_SYMB_ARROW_UP, SEG_OFF);
	display_symbol(LCD_SYMB_ARROW_DOWN, SEG_OFF);	
}

