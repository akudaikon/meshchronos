// *************************************************************************************************
#ifndef _PACKET_H_
#define _PACKET_H_

// *************************************************************************************************
// Include section
#include "project.h"

// *************************************************************************************************
// Defines section

// Packet Format
// NOTE: When changing packet, make sure to update PKT_LEN value in smartrf.h with new \
//		 packet length (in bytes)!!
typedef struct packet {
	struct {
		u8 dst		: 3;	// packet destination 		(3 bits)
		u8 src		: 3;	// packet source 			(3 bits)
		u8 type		: 2;	// packet type 				(2 bits)
	} header;				// header packet 			(total 8 bits)
	union {
		u8 raw[3];			// raw data 				(24 bits)
		struct {
			u8 xAccel;		// x-axis acceleration data (8 bits)
			u8 yAccel;		// y-axis acceleration data (8 bits)
			u8 zAccel;		// z-axis acceleration data (8 bits)
		} accel;			// accelerometer data		(total 24 bits)
	} payload; 				// payload packet 			(total 24 bits)
	struct {
		u16 rssi	: 8;
		u16 crc_ok	: 1;
		u16 lqi		: 7;
	} status;				// radio status, appended by firmware (16 bits)
} packet_t;

#define PACKET_DST_ALL 0

#define PACKET_TRANSMIT_LENGTH (sizeof(packet_t) - 2) // -2 bytes since status only appended on RX
#define PACKET_RECEIVE_LENGTH (sizeof(packet_t))

#define PACKET_TYPE_TEST (0x0)
#define PACKET_TYPE_DATA (0x1)
#define PACKET_TYPE_PING (0x2)
#define PACKET_TYPE_ACK	 (0x3)

// *************************************************************************************************
// Global Variable section
extern volatile u8 nodeNum;

#endif /*_PACKET_H_*/
