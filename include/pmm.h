#ifndef _DRIVER_PPM_H_
#define _DRIVER_PPM_H_

#include "project.h"


//====================================================================
/**
  * Set the VCore to a new level
  *
  * \param level       PMM level ID
  */
void SetVCore (unsigned char level);

//====================================================================
/**
  * Set the VCore to a new higher level
  *
  * \param level       PMM level ID
  */
void SetVCoreUp (unsigned char level);

//====================================================================
/**
  * Set the VCore to a new Lower level
  *
  * \param level       PMM level ID
  */
void SetVCoreDown (unsigned char level);

//idle
void idle(void);
void to_lpm(void);


#endif /*_DRIVER_PPM_H_*/
