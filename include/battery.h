// *************************************************************************************************

#ifndef _LOGIC_BATTERY_H_
#define _LOGIC_BATTERY_H_


// *************************************************************************************************
// Include section
#include "project.h"


// *************************************************************************************************
// Prototypes section
void reset_batt_measurement(void);
void battery_measurement(void);
void display_battery_V(void);
void clear_batteryDisplay(void);
u16 adc12_single_conversion(u16 ref, u16 sht, u16 channel);

// *************************************************************************************************
// Defines section

// Battery high voltage threshold
#define BATTERY_HIGH_THRESHOLD			(360u)

// Battery end of life voltage threshold -> disable radio, show "lobatt" message
#define BATTERY_LOW_THRESHOLD			(240u)

// Interval between battery checks (in seconds)
#define BATTERY_CHECK_INTERVAL 			(60u)


// *************************************************************************************************
// Global Variable section
struct batt
{	
	// Battery voltage
	u16			voltage;
	
	// Battery voltage offset
	s16			offset;
};
extern struct batt sBatt;


// *************************************************************************************************
// Extern section

#endif /*_LOGIC_BATTERY_H_*/
