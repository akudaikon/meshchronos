// *************************************************************************************************

#ifndef MENU_H_
#define MENU_H_

// *************************************************************************************************
// Include section


// *************************************************************************************************
// Prototypes section
void mx_idle(void);

void display_nodeNum(void);
void mx_nodeNum(void);

void display_nodeNumSelect(void);
void sx_nodeNumSelect(u8 upDown);

void display_startStopTX(void);
void mx_startStopTX(void);

void display_viewNode(void);
void mx_viewNode(void);

void display_viewNodeSelect(void);
void sx_viewNodeSelect(u8 upDown);
void mx_viewNodeSelect(void);

void display_testTX(void);
void mx_testTX(void);

void display_testTXSelect(void);
void sx_testTXSelect(u8 upDown);
void mx_testTXSelect(void);

void display_resetRadio(void);
void mx_resetRadio(void);

void nextmenu(u8 upDown);
void mx_returnToIdle(void);
void display_idle(void);
void dummy(void);

// *************************************************************************************************
// Defines section

struct menu
{
	// Pointer to menu item select function
	void (*mx_function)();		 
	// Pointer to sub menu function (change menu item, change settings)
	void (*sx_function)(u8 upDown);		 
	// Pointer to display function
	void (*display_function)();		 	 
	// Pointer to next menu item
	const struct menu *next;
	// Pointer to next prev item
	const struct menu *prev;
};


// *************************************************************************************************
// Global Variable section


// *************************************************************************************************
// Extern section

// Menu structure
extern const struct menu menu_idle;
extern const struct menu menu_nodeNum;
extern const struct menu menu_nodeNumSelect;
extern const struct menu menu_startStopTX;
extern const struct menu menu_viewNode;
extern const struct menu menu_viewNodeSelect;
extern const struct menu menu_testTX;
extern const struct menu menu_testTXSelect;
extern const struct menu menu_resetRadio;
extern const struct menu menu_battery;

// Pointers to current menu item
extern const struct menu * ptrMenu;

#endif /*MENU_H_*/
