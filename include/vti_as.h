// *************************************************************************************************

#ifndef _DRIVER_VTI_AS_H_
#define _DRIVER_VTI_AS_H_


// *************************************************************************************************
// Include section
#include "project.h"


// *************************************************************************************************
// Prototypes section
extern void as_init(void);
extern void as_start(u8 mode);
extern void as_stop(void);
extern u8 as_read_register(u8 bAddress);
extern u8 as_write_register(u8 bAddress, u8 bData);
extern void as_get_data(u8 * data);
extern u8 as_get_x(void);
extern u8 as_get_y(void);
extern u8 as_get_z(void);


// *************************************************************************************************
// Defines section
#define AS_MODE_2G_100HZ	(0x00u)
#define AS_MODE_2G_400HZ	(0x01u)
#define AS_MODE_8G_40HZ		(0x02u)
#define AS_MODE_8G_100HZ	(0x03u)
#define AS_MODE_8G_400HZ	(0x04u)


// Disconnect power supply for acceleration sensor when not used
#define AS_DISCONNECT

// Port and pin resource for SPI interface to acceleration sensor
// SDO=MOSI=P1.6, SDI=MISO=P1.5, SCK=P1.7
#define AS_SPI_IN            (P1IN)
#define AS_SPI_OUT           (P1OUT)
#define AS_SPI_DIR           (P1DIR)
#define AS_SPI_SEL           (P1SEL)
#define AS_SPI_REN           (P1REN)
#define AS_SDO_PIN           (BIT6)
#define AS_SDI_PIN           (BIT5)
#define AS_SCK_PIN           (BIT7)

// CSN=PJ.1
#define AS_CSN_OUT			 (PJOUT)
#define AS_CSN_DIR			 (PJDIR)
#define AS_CSN_PIN           (BIT1)

#define AS_TX_BUFFER         (UCA0TXBUF)
#define AS_RX_BUFFER         (UCA0RXBUF)
#define AS_TX_IFG            (UCTXIFG)
#define AS_RX_IFG            (UCRXIFG)
#define AS_IRQ_REG           (UCA0IFG) 
#define AS_SPI_CTL0          (UCA0CTL0)
#define AS_SPI_CTL1          (UCA0CTL1) 
#define AS_SPI_BR0           (UCA0BR0)
#define AS_SPI_BR1           (UCA0BR1)

// Port and pin resource for power-up of acceleration sensor, VDD=PJ.0
#define AS_PWR_OUT           (PJOUT)
#define AS_PWR_DIR           (PJDIR)
#define AS_PWR_PIN           (BIT0)

// Port, pin and interrupt resource for interrupt from acceleration sensor, CMA_INT=P2.5
#define AS_INT_IN            (P2IN)
#define AS_INT_OUT           (P2OUT)
#define AS_INT_DIR           (P2DIR)
#define AS_INT_IE            (P2IE)
#define AS_INT_IES           (P2IES)
#define AS_INT_IFG           (P2IFG)
#define AS_INT_PIN           (BIT5)

// SPI timeout to detect sensor failure
#define SPI_TIMEOUT				(1000u)


// *************************************************************************************************
// Global Variable section


// *************************************************************************************************
// Extern section


#endif /*_DRIVER_VTI_AS_H_*/
