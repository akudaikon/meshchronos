// *************************************************************************************************

#ifndef _PROJECT_H_
#define _PROJECT_H_

// *************************************************************************************************
// Include section
#include <cc430x613x.h>
#include "bm.h"


// *************************************************************************************************
// Defines section

// Choose the silicon revision of the CC430F6137 used on the watch module 
//#define CC430_IS_REV_B
#define CC430_IS_REV_C

// Comment this to not use the LCD charge pump
//#define USE_LCD_CHARGE_PUMP

// Comment this define to build the application without watchdog support
//#define USE_WATCHDOG


// *************************************************************************************************
// Macro section

// Conversion from usec to ACLK timer ticks
#define CONV_US_TO_TICKS(usec)         			(((usec) * 32768) / 1000000)

// Conversion from msec to ACLK timer ticks
#define CONV_MS_TO_TICKS(msec)         			(((msec) * 32768) / 1000) 

#define IRQ_TRIGGERED(flags, bit)				((flags & bit) == bit)


// *************************************************************************************************
// Typedef section

// Set of system flags
typedef union
{
	struct
	{
		// Various timeouts
		u16 mask_m1_button      	: 1;    // Ignore next M1 button event
		u16 mask_m2_button      	: 1;    // Ignore next M2 button event
		u16 lock_buttons			: 1;    // Lock buttons
		
		// Button auto repeat
		u16 s_button_repeat_enabled : 1;    // While in set_value(), create virtual S1/S2 button press
		
		// System messages
		u16 low_battery      		: 1;    // 1 = Battery is low
		u16 delay_over     			: 1;    // 1 = Timer delay over
		
		// System flags
		u16 tx_active				: 1;	// 1 = Accel. TX mode active
	} flag;
	u16 all_flags;            // Shortcut to all display flags (for reset)
} s_system_flags;
extern volatile s_system_flags sys;


// Set of request flags
typedef union
{
	struct
	{
		u16 temperature_measurement 	: 1;    // 1 = Measure temperature
		u16 voltage_measurement    		: 1;    // 1 = Measure voltage
		u16	acceleration_measurement	: 1; 	// 1 = Measure acceleration
		u16 radio_received				: 1;	// 1 = packet received
		u16 radio_transmitted			: 1;	// 1 = packet transmitted
		u16 radio_ack					: 1;	// 1 = ack received
	} flag;
	u16 all_flags;            // Shortcut to all display flags (for reset)
} s_request_flags;
extern volatile s_request_flags request;


typedef struct
{
	u8 info_length;
	u8 crc_length;
	u16 crc_value;
	u16 device_id;
	struct
	{
		u16 fw_major : 4;
		u16 fw_minor : 4;
		u16 hw_major : 4;
		u16 hw_minor : 4;
	} revision;
} device_t;
extern device_t device;

// *************************************************************************************************
// Global Variable section


#endif /*_PROJECT_H_*/
