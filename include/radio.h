// *************************************************************************************************

#ifndef _DRIVER_RADIO_H_
#define _DRIVER_RADIO_H_

#include "project.h"

extern void radio_config(void);
extern void radio_reset(void);
extern void radio_powerdown(void);
extern void radio_start(void);
extern void radio_stop(void);
extern void radio_transmit(u8 *data, u8 length);
extern void radio_receive(u8 *data, u8 length);
extern void radio_transmitted(void);
extern void radio_received(void);
extern void radio_receive_on(void);
extern u8 radio_status(void);
extern void display_signalStrength(u8 rssi);
extern void clear_signalStrength(void);
extern void radio_sendTest(u8 dstNode, u8 testData);
extern void radio_sendAccel(void);
extern void radio_sendPing(u8 dstNode);
extern void radio_sendAck(u8 dstNode);

#endif /*_DRIVER_RADIO_H_*/
