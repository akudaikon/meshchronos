// *************************************************************************************************

#ifndef _LOGIC_ACCELERATION_H_
#define _LOGIC_ACCELERATION_H_


// *************************************************************************************************
// Include section
#include "project.h"


// *************************************************************************************************
// Prototypes section



// *************************************************************************************************
// Defines section
#define DISPLAY_ACCEL_X		(0u)
#define DISPLAY_ACCEL_Y		(1u)
#define DISPLAY_ACCEL_Z		(2u)

#define ACCEL_MODE_OFF		(0u)
#define ACCEL_MODE_ON		(1u)


// *************************************************************************************************
// Global Variable section
struct accel
{
	// ACC_MODE_OFF, ACC_MODE_ON
	u8			mode;
	
	// Sensor raw data
	u8			xyz[3];

	// Acceleration data in 10 * mgrav
	u16			data;

	// Display X/Y/Z values	
	u8 			view_style;
};
extern struct accel sAccel;


// *************************************************************************************************
// Extern section
extern void reset_acceleration(void);
extern void switchAccelDisplay(void);
extern u8 is_acceleration_measurement(void);
extern void do_acceleration_measurement(void);
extern void display_acceleration(u8 xData, u8 yData, u8 zData);
extern void clear_accelDisplay(void);

#endif /*_LOGIC_ACCELERATION_H_*/
