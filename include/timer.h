// *************************************************************************************************

#ifndef _DRIVER_TIMER_H_
#define _DRIVER_TIMER_H_

// *************************************************************************************************
// Include section
#include "project.h"


// *************************************************************************************************
// Prototypes section
extern void Timer0_Init(void);
extern void Timer0_Start(void);
extern void Timer0_Stop(void);
extern void Timer0_A1_Start(u16 ticks);
extern void Timer0_A1_Stop(void);
extern void Timer0_A3_Start(u16 ticks);
extern void Timer0_A3_Stop(void);
extern void Timer0_A4_Delay(u16 ticks);



// *************************************************************************************************
// Defines section
struct timer
{
	// Timer0_A1 periodic delay
	u16		timer0_A1_ticks;
	// Timer0_A3 periodic delay 
	u16		timer0_A3_ticks;
};

extern struct timer sTimer;


// *************************************************************************************************
// Global Variable section


// *************************************************************************************************
// Extern section



#endif /*_DRIVER_TIMER_H_*/

