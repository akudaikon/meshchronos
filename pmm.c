//****************************************************************************//
// Function Library for setting the PMM
//****************************************************************************//

#include "project.h"
#include "pmm.h"


//****************************************************************************//
// Set VCore level
// SetVCore level is called from the user API 
//****************************************************************************//
void SetVCore (unsigned char level)        // Note: change level by one step only
{
  unsigned char actLevel;
  do {
    actLevel = PMMCTL0_L & PMMCOREV_3;
    if (actLevel < level) 
      SetVCoreUp(++actLevel);               // Set VCore (step by step)
    if (actLevel > level) 
      SetVCoreDown(--actLevel);             // Set VCore (step by step)
  }while (actLevel != level);
}


//****************************************************************************//
// Set VCore up 
//****************************************************************************//
void SetVCoreUp (unsigned char level)        // Note: change level by one step only
{
  PMMCTL0_H = 0xA5;                         // Open PMM module registers for write access
 
  SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level;     // Set SVS/M high side to new level

  SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level;     // Set SVM new Level    
  while ((PMMIFG & SVSMLDLYIFG) == 0);      // Wait till SVM is settled (Delay)
  PMMCTL0_L = PMMCOREV0 * level;            // Set VCore to x
  PMMIFG &= ~(SVMLVLRIFG + SVMLIFG);        // Clear already set flags
  if ((PMMIFG & SVMLIFG))
    while ((PMMIFG & SVMLVLRIFG) == 0);     // Wait till level is reached
 
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;     // Set SVS/M Low side to new level
  PMMCTL0_H = 0x00;                         // Lock PMM module registers for write access
}



//****************************************************************************//
// Set VCore down 
//****************************************************************************//
void SetVCoreDown (unsigned char level)
{
  PMMCTL0_H = 0xA5;                         // Open PMM module registers for write access
  SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0 * level;     // Set SVS/M Low side to new level
  while ((PMMIFG & SVSMLDLYIFG) == 0);      // Wait till SVM is settled (Delay)
  PMMCTL0_L = (level * PMMCOREV0);          // Set VCore to 1.85 V for Max Speed.
  PMMCTL0_H = 0x00;                         // Lock PMM module registers for write access
}


//****************************************************************************//

//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
//IDLE
//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------

void idle(void)
{
	// To low power mode
	to_lpm();

#ifdef USE_WATCHDOG		
	// Service watchdog
	WDTCTL = WDTPW + WDTIS__512K + WDTSSEL__ACLK + WDTCNTCL;
#endif
}

// *************************************************************************************************
// @fn          to_lpm
// @brief       Go to LPM0/3. 
// @param       none
// @return      none
// *************************************************************************************************
void to_lpm(void)
{
#ifdef CC430_IS_REV_B
	u8 state;

	// Go to LPM3 whenever radio is IDLE
	// Go to LPM3 when radio is in some RX or TX mode
	state = rf1a_strobe(RF_SNOP);
	
	if ((state & 0x70) == 0)
	{
		// radio in IDLE state
		_BIS_SR(LPM3_bits + GIE); 
		__no_operation();	
	}
	else
	{
		// radio is active
		_BIS_SR(LPM0_bits + GIE); 
		__no_operation();	
	}
#endif

#ifdef CC430_IS_REV_C
	// Go to LPM3
	_BIS_SR(LPM3_bits + GIE); 
	__no_operation();
#endif
}
