// *************************************************************************************************
// Menu management functions.
// *************************************************************************************************

// *************************************************************************************************
// Include section
#include "project.h"
#include "display.h"
#include "menu.h"
#include "packet.h"
#include "radio.h"
#include "pmm.h"
#include "vti_as.h"
#include "acceleration.h"
#include "ports.h"
#include "battery.h"

// *************************************************************************************************
// Defines section
#define FUNCTION(function)  function

// *************************************************************************************************
// Global Variable section
const struct menu * ptrMenu = NULL;
u8 testNode = 1;
u8 viewNode = 1;

// *************************************************************************************************
// User navigation ( [____] = default menu item after reset )
//
//	[IDLE] -> Node Number -> Start/Stop TX -> View Node -> Test TX -> Reset Radio -> View Batt 
// *************************************************************************************************

// Idle Screen
const struct menu menu_idle =
{
	FUNCTION(mx_idle),				// menu item select function
	FUNCTION(nextmenu),				// sub menu function
	FUNCTION(display_idle),			// display function
	&menu_nodeNum,					// next menu
	&menu_battery					// prev menu
};

// Node Number Menu
const struct menu menu_nodeNum =
{
	FUNCTION(mx_nodeNum),			// menu item select funcion
	FUNCTION(nextmenu),				// sub menu function
	FUNCTION(display_nodeNum),		// display function
	&menu_startStopTX,				// next menu
	&menu_idle						// prev menu
};

// Node Number Select Menu
const struct menu menu_nodeNumSelect =
{
	FUNCTION(mx_returnToIdle),		// menu item select funcion
	FUNCTION(sx_nodeNumSelect),		// sub menu function
	FUNCTION(display_nodeNumSelect),// display function
	&menu_idle,						// next menu
	&menu_idle						// prev menu
};

// Start/Stop TX Menu
const struct menu menu_startStopTX =
{
	FUNCTION(mx_startStopTX),			// menu item select function
	FUNCTION(nextmenu),					// sub menu function
	FUNCTION(display_startStopTX),		// display function
	&menu_viewNode,						// next menu
	&menu_nodeNum						// prev menu
};

// View Node Menu
const struct menu menu_viewNode =
{
	FUNCTION(mx_viewNode),			// menu item select function
	FUNCTION(nextmenu),				// sub menu function
	FUNCTION(display_viewNode),		// display function
	&menu_testTX,					// next menu
	&menu_startStopTX				// prev menu
};

// View Node Node Select Menu
const struct menu menu_viewNodeSelect =
{
	FUNCTION(mx_viewNodeSelect),		// menu item select function
	FUNCTION(sx_viewNodeSelect),		// sub menu function
	FUNCTION(display_viewNodeSelect),	// display function
	&menu_idle,							// next menu
	&menu_idle							// prev menu
};

// Test TX Menu
const struct menu menu_testTX =
{
	FUNCTION(mx_testTX),			// menu item select function
	FUNCTION(nextmenu),				// sub menu function
	FUNCTION(display_testTX),		// display function
	&menu_resetRadio,				// next menu
	&menu_viewNode					// prev menu
};

// Test TX Node Select Menu
const struct menu menu_testTXSelect =
{
	FUNCTION(mx_testTXSelect),		// menu item select function
	FUNCTION(sx_testTXSelect),		// sub menu function
	FUNCTION(display_testTXSelect),	// display function
	&menu_idle,						// next menu
	&menu_idle						// prev menu
};

// Reset Radio Menu
const struct menu menu_resetRadio =
{
	FUNCTION(mx_resetRadio),		// menu item select function
	FUNCTION(nextmenu),				// sub menu function
	FUNCTION(display_resetRadio),	// display function
	&menu_battery,					// next menu
	&menu_testTX					// prev menu
};

// Battery Status Menu
const struct menu menu_battery =
{
	FUNCTION(dummy),				// menu item select function
	FUNCTION(nextmenu),				// sub menu function
	FUNCTION(display_battery_V),	// display function
	&menu_idle,						// next menu
	&menu_resetRadio				// prev menu
};


//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
// MENU FUNCTIONS
//-------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------
void nextmenu(u8 upDown)
{
	if (upDown == 1) ptrMenu = ptrMenu->next;
	else if (upDown == 2) ptrMenu = ptrMenu->prev;
	
	clear_accelDisplay();
	clear_batteryDisplay();
	ptrMenu->display_function();
}

void mx_returnToIdle()
{
	ptrMenu = &menu_idle;
	ptrMenu->display_function();	
}

void display_idle()
{
	clear_display();
	clear_batteryDisplay();
	clear_accelDisplay();
	
	if (sys.flag.tx_active)
	{
		display_acceleration(sAccel.xyz[0], sAccel.xyz[1], sAccel.xyz[2]);
		display_chars(LCD_SEG_L2_4_0, (u8*)"RUN  ", SEG_ON);
		display_symbol(LCD_ICON_BEEPER1, SEG_ON);
		display_symbol(LCD_ICON_BEEPER2, SEG_ON);
		display_symbol(LCD_ICON_BEEPER3, SEG_ON);
					
	} 
	else 
	{
		display_chars(LCD_SEG_L1_3_0, (u8*)"    ", SEG_ON);
		display_chars(LCD_SEG_L2_4_0, (u8*)"IDLE ", SEG_ON);
		display_symbol(LCD_ICON_BEEPER1, SEG_OFF);
		display_symbol(LCD_ICON_BEEPER2, SEG_OFF);
		display_symbol(LCD_ICON_BEEPER3, SEG_OFF);
	}		
}

void mx_idle()
{
	if (sys.flag.tx_active) switchAccelDisplay();	
}

void dummy()
{
	// Do nothing!
}

// *************************************************************************************************
// nodeNum Menu
void display_nodeNum()
{
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"NODE", SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)"ADDR ", SEG_ON);
}

void mx_nodeNum()
{
	ptrMenu = &menu_nodeNumSelect;
	ptrMenu->display_function();
}

// *************************************************************************************************
// nodeNumSelect Menu
void display_nodeNumSelect()
{
	u8 * str;
	str = itoa(nodeNum, 2, 0);
	
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"NODE", SEG_ON);
	display_chars(LCD_SEG_L2_1_0, str, SEG_ON);
}

void sx_nodeNumSelect(u8 upDown)
{
	// Up button pressed
	if (upDown == 1)
	{
		if (++nodeNum > 15) nodeNum = 1;
	} 
	// Down button pressed
	else if (upDown == 2)
	{
		if (--nodeNum < 1) nodeNum = 15;
	}
	
	// Update display with new node number
	display_nodeNumSelect();
}

// *************************************************************************************************
// startStopTX Menu
void display_startStopTX()
{
	clear_display();
	
	if (sys.flag.tx_active)
	{
		display_chars(LCD_SEG_L1_3_0, (u8*)" TX ", SEG_ON);
		display_chars(LCD_SEG_L2_4_0, (u8*)"STOP ", SEG_ON);
	}
	else
	{
		display_chars(LCD_SEG_L1_3_0, (u8*)" TX ", SEG_ON);
		display_chars(LCD_SEG_L2_4_0, (u8*)"START", SEG_ON);
	}
}

void mx_startStopTX()
{
	// Toggle accelerometer TXing
	sys.flag.tx_active ^= 1;
	
	if (sys.flag.tx_active)
	{	
		// Start the accelerometer
		as_start(AS_MODE_2G_100HZ);
		sAccel.mode = ACCEL_MODE_ON;
	}
	else
	{
		// Stop the accelerometer
		as_stop();
		sAccel.mode = ACCEL_MODE_OFF;
		
		// Clear display of accelerometer bits
		clear_accelDisplay();
	}
	
	mx_returnToIdle();
}

// *************************************************************************************************
// viewNode Menu
void display_viewNode()
{
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"VIEW", SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)"NODE ", SEG_ON);
}

void mx_viewNode()
{
	ptrMenu = &menu_viewNodeSelect;
	ptrMenu->display_function();
}

// *************************************************************************************************
// viewNodeSelect Menu
void display_viewNodeSelect()
{
	u8 * str;
	str = itoa(viewNode, 2, 0);
	
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"NODE", SEG_ON);
	display_chars(LCD_SEG_L2_1_0, str, SEG_ON);
}

void sx_viewNodeSelect(u8 upDown)
{
	// Up button pressed
	if (upDown == 1)
	{
		if (++viewNode > 15) viewNode = 1;
	} 
	// Down button pressed
	else if (upDown == 2)
	{
		if (--viewNode < 1) viewNode = 15;
	}
	
	// Update display with new node number
	display_viewNodeSelect();
}

void mx_viewNodeSelect()
{
	u8 * strSrc;
	packet_t packet = {0};

	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"DATA", SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)"WAIT ", SEG_ON);
	
	radio_start();
				
	for(;;)
	{
		idle();
		
		// check for M1	
		if(button.flag.m1) 
		{
			// switch displayed accelerometer axis
			switchAccelDisplay();
						
			// Clear button flag
			button.flag.m1 = 0;
		}	
		// check for M2
		else if(button.flag.m2) 
		{
			// return back to idle screen
			mx_returnToIdle();
						
			// Clear button flag
			button.flag.m2 = 0;
			
			// exit loop
			break;
		}
		
		// packet received interrupt
		if (request.flag.radio_received)
		{			
			radio_receive((u8*)&packet, PACKET_RECEIVE_LENGTH);
			
			// Is this a packet from the node we're viewing and the correct type?
			if (packet.header.src == viewNode && packet.header.type == PACKET_TYPE_DATA)
			{
				// Check to see if CRC is ok, otherwise display error
				// TODO: CRC doesn't seem to be working correctly??
//				if (packet.status.crc_ok)
//				{
					display_acceleration(packet.payload.accel.xAccel, packet.payload.accel.yAccel,
										 packet.payload.accel.zAccel);
					display_chars(LCD_SEG_L2_4_0, (u8*)"RX   ", SEG_ON);
					strSrc = itoa(viewNode, 2, 0);
					display_chars(LCD_SEG_L2_1_0, strSrc, SEG_ON);
//				}	
//				else
//				{
//					clear_accelDisplay();
//					display_chars(LCD_SEG_L1_3_1, (u8*)"ERR ", SEG_ON);
//				}
	
				// display signal strength of received packet with LCD radio segments
				display_signalStrength(packet.status.rssi);
			}
		}	
		// Clear the RX flag and reset the radio
		request.flag.radio_received = 0;
		radio_start();			
	}
	
	// clear display of accelerometer and signal strength bits
	clear_accelDisplay();
	clear_signalStrength();
	
	// Just in case we broke out of the for loop before doing this...
	request.flag.radio_received = 0;
	radio_start();
}

// *************************************************************************************************
// testTX Menu
void display_testTX()
{
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"TEST", SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)" TX  ", SEG_ON);
}

void mx_testTX()
{
	ptrMenu = &menu_testTXSelect;
	ptrMenu->display_function();
}

// *************************************************************************************************
// testTXSelect Menu
void display_testTXSelect()
{
	u8 * str;
	str = itoa(testNode, 2, 0);
	
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"NODE", SEG_ON);
	display_chars(LCD_SEG_L2_1_0, str, SEG_ON);
}

void sx_testTXSelect(u8 upDown)
{
	// Up button pressed
	if (upDown == 1)
	{
		if (++testNode > 15) testNode = 1;
	} 
	// Down button pressed
	else if (upDown == 2)
	{
		if (--testNode < 1) testNode = 15;
	}
	
	// update display with new node number
	display_testTXSelect();
}

void mx_testTXSelect()
{
	u8 * strSent;
	u8 * strDst;
	u8 testData = TA0R_L;

	// Send a test packet with a "random" number to selected node number
	radio_sendTest(testNode, testData);
	
	// display sent number along with destination node number
	strSent = itoa(testData, 4, 0);
	display_chars(LCD_SEG_L1_3_0, strSent, SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)"TX   ", SEG_ON);
	strDst = itoa(testNode, 2, 0);
	display_chars(LCD_SEG_L2_1_0, strDst, SEG_ON);
}

// *************************************************************************************************
// resetRadio Menu
void display_resetRadio()
{
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"RDIO", SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)"RESET", SEG_ON);
}

void mx_resetRadio()
{
	request.flag.radio_received = 0;
	request.flag.radio_transmitted = 0;
	radio_stop();
	radio_start();
	
	clear_display();
	display_chars(LCD_SEG_L1_3_0, (u8*)"DONE", SEG_ON);
	display_chars(LCD_SEG_L2_4_0, (u8*)"RESET", SEG_ON);
}
